FROM php:8.0-fpm

ENV ACCEPT_EULA=Y

# Add deb for Symfony CLI & NodJS
RUN echo 'deb [trusted=yes] https://repo.symfony.com/apt/ /' | tee /etc/apt/sources.list.d/symfony-cli.list
RUN curl -fsSL https://deb.nodesource.com/setup_18.x | bash -
# Apt
RUN apt update && apt install -y git symfony-cli nodejs gnupg2 librabbitmq-dev zlib1g-dev libpng-dev libicu-dev libxslt1-dev libzip-dev libpq-dev libssl-dev

# Node.js
RUN ln -s /usr/bin/nodejs /usr/local/bin/node
RUN ln -s /usr/bin/npm /usr/local/bin/npm

# Yarn
RUN npm install -g yarn

# Extensions PHP
RUN pecl install amqp apcu redis xdebug
RUN docker-php-ext-enable amqp apcu redis xdebug
RUN docker-php-ext-install gd intl opcache pdo pdo_pgsql xsl zip

# Composer
COPY --from=composer /usr/bin/composer /usr/bin/composer

# Config git
RUN git config --global user.email "example@mail.com"
RUN git config --global user.name "example"

CMD ["php-fpm"]

EXPOSE 9000