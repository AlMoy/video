# Video
The **Video** application allows to add videos and place them in playlists.
## Docker compose
The environment is composed of PHP 8.0, PostgresSQL and Nginx.\
Create the environnement for application Symfony **Video**, with the command :
```shell
docker-compose up -d
```
For connect at container **PHP**, enter the command :
```shell
docker exec -ti php-fpm /bin/bash
```
## Start project
Create docker environnement and connect at container **PHP**.\
Launch the next commands :
```shell
cd video
composer update
yarn install
yarn encore prod
symfony console doctrine:migrations:migrate
```
And edit the file `.env`, change the value of environnement variable `APP_ENV` by value `prod`