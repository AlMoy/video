let input = document.getElementById('playlist'),
    modal = new bootstrap.Modal('#playlistModal'),
    section = document.getElementById('playlists');

document
    .getElementById('create-playlist')
    .onclick = submit;

function submit() {
    let title = input.value.trim();

    if (title === "")
        return;

    let init = {
        method: 'POST',
        headers: new Headers({ "Content-Type": "application/json" }),
        body: JSON.stringify({ title: title, createAt: new Date() })
    };

    fetch('/api/playlists.json', init)
        .then(response => {
            if (response.ok) {
                modal.hide();
                input.value = "";

                response
                    .json()
                    .then(json => {
                        let card = document.createElement('div'),
                            body = document.createElement('div'),
                            title = document.createElement('a'),
                            textNumber = document.createElement('p'),
                            textCreateAt = document.createElement('p');

                        card.classList.add('card', 'col-12', 'col-md-6', 'col-lg-4', 'col-xl-2');
                        card.append(body);

                        body.classList.add('card-body');
                        body.append(title);
                        body.append(textNumber);
                        body.append(textCreateAt);

                        title.textContent = json.title;
                        title.href = section.dataset.href.replace('id', json.id);
                        title.classList.add('card-title');

                        textNumber.textContent = '{text} : {data}'
                            .replace('{text}', section.dataset.numberVideo)
                            .replace('{data}', json.videos.length);
                        textNumber.classList.add('card-text', 'mb-0');

                        textCreateAt.textContent = '{text} : {data}'
                            .replace('{text}', section.dataset.createAt)
                            .replace('{data}', new Date(json.createAt).toLocaleDateString());
                        textCreateAt.classList.add('card-text', 'mb-0');

                        section.append(card);
                    })
            }
        })
}