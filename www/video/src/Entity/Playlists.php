<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PlaylistsRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: PlaylistsRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get' => ['normalization_context' => ['groups' => 'playlists:list']],
        'post' => [
            'normalization_context' => ['groups' => 'playlists:list'],
            'denormalization_context' => ['groups' => 'playlists:post']
        ]
    ],
    itemOperations: [
        'get' => ['normalization_context' => ['groups' => 'playlists:item']],
        'delete' => ['requirements' => ['id' => '\d+']]
    ]
)]
class Playlists
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['playlists:list', 'playlists:item'])]
    private ?int $id = null;

    #[ORM\Column(length: 40)]
    #[Groups(['playlists:list', 'playlists:item', 'playlists:post'])]
    private ?string $title = null;

    #[ORM\ManyToMany(targetEntity: Videos::class, inversedBy: 'playlists')]
    #[Groups(['playlists:list', 'playlists:item'])]
    private Collection $videos;

    #[ORM\Column]
    #[Groups(['playlists:list', 'playlists:item', 'playlists:post'])]
    private ?DateTimeImmutable $createAt = null;

    public function __construct()
    {
        $this->videos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection<int, Videos>
     */
    public function getVideos(): Collection
    {
        return $this->videos;
    }

    public function addVideo(Videos $video): self
    {
        if (!$this->videos->contains($video)) {
            $this->videos->add($video);
        }

        return $this;
    }

    public function removeVideo(Videos $video): self
    {
        $this->videos->removeElement($video);

        return $this;
    }

    public function getCreateAt(): ?DateTimeImmutable
    {
        return $this->createAt;
    }

    public function setCreateAt(DateTimeImmutable $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }
}
