<?php

namespace App\EventListener;

use App\Entity\Videos;
use DateTimeImmutable;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Constraints\File;

class VideosFormSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            FormEvents::PRE_SET_DATA => 'onPreSetData',
            FormEvents::POST_SUBMIT => 'onPostSubmit'
        ];
    }

    public function onPreSetData(FormEvent $event): void
    {
        if (is_null($event->getData()->getId()))
            $event
                ->getForm()
                ->add('file', FileType::class, [
                    'constraints' => [
                        new File([
                            'mimeTypes' => 'video/mp4',
                            'maxSize' => '10Mi'
                        ])
                    ],
                    'attr' => [
                        'accept' => 'video/mp4'
                    ]
                ]);
    }

    public function onPostSubmit(FormEvent $event): void
    {
        /** @var Videos $video */
        $video = $event->getData();
        $form = $event->getForm();

        if ($form->isValid()) {
            if (is_null($video->getCreateAt()))
                $video->setCreateAt(new DateTimeImmutable());

            if ($form->has('file')) {
                $video->setFile(fopen($form->get('file')->getData(), 'r'));
            }

            $playlists = clone $form->get('playlists')->getData();
            $video->getPlaylists()->clear();
            foreach ($playlists as $playlist)
                $video->addPlaylist($playlist);
        }
    }
}