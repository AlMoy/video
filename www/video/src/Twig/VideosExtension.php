<?php

namespace App\Twig;

use App\Entity\Videos;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class VideosExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('base64', [$this, 'base64'])
        ];
    }

    public function base64(Videos $video): string
    {
        return base64_encode(stream_get_contents($video->getFile()));
    }
}