<?php

namespace App\Controller;

use App\Entity\Playlists;
use App\Entity\Videos;
use App\Repository\PlaylistsRepository;
use App\Repository\VideosRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(VideosRepository $videosRepository): Response
    {
        return $this->render('home/index.html.twig', [
            'videos' => $videosRepository->findAll(),
        ]);
    }

    #[Route('/playlists', name: 'app_playlists')]
    public function playlists(PlaylistsRepository $playlistsRepository): Response
    {
        return $this->render('home/playlists.html.twig', [
            'playlists' => $playlistsRepository->findAll(),
        ]);
    }

    #[Route('/{id}/playlists', name: 'app_playlists_show')]
    public function playlist(Playlists $playlists): Response
    {
        return $this->render('home/playlist.html.twig', [
            'playlists' => $playlists,
        ]);
    }

    #[Route('/{id}/videos', name: 'app_video')]
    public function video(Videos $videos): Response
    {
        return $this->render('home/video.html.twig', [
            'videos' => $videos,
        ]);
    }
}
